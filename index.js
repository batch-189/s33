fetch('https://jsonplaceholder.typicode.com/todos')
			.then((response) => response.json())
			.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	data.map((toDo) => {
		 console.log(toDo.title)
	})
})


fetch('https://jsonplaceholder.typicode.com/todos', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				userId: 1,
				title: 'Created To DO list Item',
				
			})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				dateCompleted: 'Pending',
				description: 'To update the my to do list with a different structure',
				userId: 1,
				title: 'Updated To do List item',
				status: 'Pending'
				
			})
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				dateCompleted: '07/09/21',
				description: 'To update the my to do list with a different structure',
				userId: 1,
				title: 'delectus aut auten',
				status: 'Status'
				
			})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE',
	
}).then((response) => response.json())
.then((data) => console.log(data))



